import { useState } from 'react';
import { Navigate } from 'react-router-dom';
import { useToken } from '../../TokenContext';

import './Login.css'

export const Login = () =>{
    const [token, setToken] = useToken();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);

    if (token) return <Navigate to='/' />;
    
    const handleLogin = async (e) => {
        e.preventDefault();

        setLoading(true);

        try {
            const res = await fetch('http://localhost:4000/users/login', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email,
                    password,
                }),
            });

            const body = await res.json();

            if (body.status === 'error') {
                alert(body.message);
            } else {
                setToken(body.data.token);
            }
        } catch (err) {
            console.error(err);
        } finally {
            setLoading(false);
        }
    };

    return (
        <main className='login'>
            <form onSubmit={handleLogin}>
                <label htmlFor='email'>Email:</label>
                <input
                    type='email'
                    id='email'
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
                <label htmlFor='pass'>Contraseña:</label>
                <input
                    type='password'
                    id='pass'
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    minLength='6'
                    required
                />

                <button disabled={loading}>Login</button>
            </form>
        </main>
    )
}