import { useState } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { useToken } from '../../TokenContext';

import './Register.css'

export const Register = () => {
    const [token, setToken] = useToken()
    const navigate = useNavigate()
    const [loading, setLoading] = useState(false)

    const [password, setPassword] = useState('')
    const [email, setEmail] = useState('')

    if (token) return <Navigate to='/' />;

    const handleSubmit = async (e) => {
        e.preventDefault()

        setLoading(true)

        try {
            const res = await fetch('http://localhost:4000/users', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email,
                    password,
                }),
            });

            const body = await res.json();

            if (body.status === 'error') {
                alert(body.message);
            } else {
                navigate('/login');
            }
        } catch (err) {
            console.error(err);
        } finally {
            setLoading(false);
        }
    }
    return(
            <main className='register'>
                <form onSubmit={handleSubmit}>
                    <label htmlFor='email'>Email:</label>
                    <input
                        type='email'
                        id='email'
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                    <label htmlFor='pass'>Contraseña:</label>
                    <input
                        type='password'
                        id='pass'
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        minLength='6'
                        required
                    />
    
                    <button disabled={loading}>Registrarse</button>
                </form>
            </main>
        )
}